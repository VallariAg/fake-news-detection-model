# Fake News Detection Model

A tool for detecting the language patterns that characterize fake and real news through the use of machine learning

## Abstract

Recent events have led to an increase in the popularity and spread of fake news. As demonstrated by the widespread effects of the large onset of fake news, humans are inconsistent if not outright poor detectors of fake news. 
Our project aims to use Natural Language Processing to detect fake news directly, based on the text content of news articles.

## Overview

### MODELS to apply:
    (planned so far)
    - Naive Bayes 
    - SVM
    - Decision Tree
    - Logistic Regression

### Output
    (add here later)

## Dataset - 
- [Liar Liar Pants on Fire](https://www.cs.ucsb.edu/~william/data/liar_dataset.zip)

### Dataset Description
- Training: It has following attributes:
    - title
    - author
    - text
- Testing: same attributes


## References:
- [Fake News Detection using ML by Simon Lorent](https://matheo.uliege.be/bitstream/2268.2/8416/1/s134450_fake_news_detection_using_machine_learning.pdf)
- [ML for Fake News Detection by Nicole O’ Brein](https://dspace.mit.edu/bitstream/handle/1721.1/119727/1078649610-MIT.pdf)

## Authors

- [Malaika Agrawal](https://gitlab.com/malaika2820)
- [Vallari Agrawal](https://gitlab.com/vallariag)
- [Tania Malhotra](https://gitlab.com/taniamalhotra)



